[![codecov](https://codecov.io/gl/yassine.ameur.ext/poc-datalake/branch/master/graph/badge.svg?token=9CEOWWRVQP)](undefined)


### Context:
This mini project is written for a demo purpose. It aims at making a simple poc to serve as discussions basis for the new datalake project.

### Description:
This project aims at :
- Reproducing the same approach (simplified) designed by veolia for its datalake project: 
  - Start point: a json that contains all the info that a dag need to be built (The dag is in slave mode)
  - Generate a dag from the json
  - the only difference with the initial approach is that we do not need one `.py` file for each dag: One parametrized dag file will do the job.
- Each dag does the following: load a csv file from GCS, put it into a temporary table, apply some transformations and upload them into a table.
- An additional fake dag is coded for learning purpose:  To illustrate how to use custom plugins (operators and hooks)
- Serve as a sandbox for developers: run airflow locally, run tests, define dependencies

## Launch the project locally:

### Prerequisites:
- Docker installed on your machine (use a recent version > 3.2)

### Steps

#### Launch services:
To launch the project, please use this command: `docker-compose up -d`

PS: Please note, that if it is your first run, this will take many minutes (Take your cup and go to coffee machine).

This command will create the following containers:

- datalake_webserver: Airflow UI (webserver), you can access it through [localhost:8080](localhost:8080)
- postgres (For metadata): you can see the content of database on this uri: `postgresql://airflow:airflow@localhot:5432/airflow`
- redis: the broker that will be used to push tasks
- datalake_scheduler
- datalake_worker
- datalake_flower: To monitor your workers, you can access it through [localhost:5555](localhost:5555)


#### Check services:
In order to check that all services are well working, please run this command: `docker ps`

As an output, you should see all the above mentioned containers with the status `UP` 

#### Shutdown services
When you finish working with airflow, I invite you to shut it down: `docker-compose down`


### Developer section:
If you are a developer and want to contribute, you are welcome. Here is what you have to do:
- Edit the code and make your contribution.
- Run tests: `tox` (you have to install tox before: pip install tox). This will do the following:
   1) run `isort`: check that all your imports respect pep8 convention.
   2) run `flake` to test your code's cleanliness.
   3) run `mypy`: to test that you respect signatures.
   3) un `pytest`: check that your unittests are working (Please, maintain code coverage at 100%, this mandatory to project credibility)  

- Open airflow and check the effect of your contribution
